<?php
	require_once 'backend/Views/display_nav.php';
	require_once 'backend/Views/display_menu.php';
	require_once 'backend/Controllers/app_config.php';

	session_start();
	if(isset($_SESSION['user_id'])){
		require_once 'backend/Controllers/database_connection.php';
		require_once 'backend/Controllers/authorize.php';
	}

	$ruy_lopez = "backend/Controllers/show_game.php?move_id=".RUY_LOPEZ;
	$italian_game = "backend/Controllers/show_game.php?move_id=".ITALIAN_GAME;
	$open_sicilian = "backend/Controllers/show_game.php?move_id=".OPEN_SICILIAN;
?>


<html>
<head>
	<link href="frontend/css/backend_styles.css" rel="stylesheet" />
	<title>Welcome to ChessClubHub</title>
</head>
<body>
	<div id="content_container">
		<div id="header"><img src="../../frontend/img/ChessClubHub.png" width="200" height="50" alt="header" /></div>
		<div id="sidebar"> <?php display_nav(); ?></div> <!--end #sidebar-->
		<div id="mainContent">
			<?php 
			if(isset($_SESSION['user_id'])){
				display_menu($link); 
			} else {
				echo "<div id='menu'><ul><li><a href='/auth'>Sign In</a></li></ul></div>";
			}
			?>

			<main id="main">
			<a href=<?php echo $ruy_lopez; ?> ><h4> Ruy Lopez <p class="photo floatRight"> <img src="frontend/img/ChessHub_example.png" width="150" height="130" alt="Ruy Lopez" />Jump to Ruy Lopez.</p></h4></a>
			<p>The Ruy Lopez (1.e4 e5, 2.Nf3 Nc3, 3.Bb5 ...) is designed to put early pressure on the Black knight at Nc6. The pressure can result in the knight failing to defend the e5 pawn, either in an <b>exchange</b> or a <b>pin</b>, so the white knight can take the pawn at 4. Nxe5 ...</p>
			<br class="clearfloat" />

			<a href=<?php echo $italian_game; ?>><h4> Italian Game <p class="photo floatRight"> <img src="frontend/img/ChessHub_example.png" width="150" height="130" alt="alpacas" /> Jump to Italian Game. </p></h4></a>
			<p>The Italian Game (1.e4 e5, 2.Nf3 Nc3, 3.Bc4 ...) is designed to put early pressure on Black's f7 pawn. The White knight can eventually move to 7.g5 to get a <b>bridge</b> at 8.Nxf7 or a <b>check</b> at 8.Bxf7.</p>
			<br class="clearfloat" />
			
			<a href=<?php echo $open_sicilian; ?>><h4> Open Sicilian <p class="photo floatRight"> <img src="frontend/img/ChessHub_example.png" width="150" height="130" alt="alpacas" /> Jump to Sicilian. </p></h4></a>
			<p>The Open Sicilian (1.e4 e5, 2.Nf3 Nc3, 3.Bb5 ...) is an asymmetrical game designed to put white off balance. While White is given the opportunity to advance the 'e4' pawn forward, black is developing a position to <b>stack attack</b> with an eventual 7. ... Qc7 and 8. ... Bd6 or 8. ... Rc8</p>
			</main>
			<br class="clearfloat" />
		</div> <!--end mainContent-->
		<div id="footer">
				<p id="copyright">copyright 2016 ChessClubHub Services, Inc. All rights reserved.</p>
			</div> <!-- end #footer -->

	</div><!--end content_container-->

</body>
</html