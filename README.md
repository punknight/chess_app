<h1>New Goals (4/5/2016)</h1>
Goal 1 - deploy the website so that users can start tracking their five best openings
Goal 2 - kickstarter campaign to give the same website directed to SC2 and LoL or other games
Goal 3 - patent

Subgoals

A. Create User Flow - DONE (except for last page wihich is part of goal C)
B. Authenticate User Flow 
	A failed test for authentication flow is allowing a user to see another user's id.

C. Upload Opening Flow - causes the opening to fall under the heading for the five main openings.
	i. Uploading a game is simple at first, just allowing users to save a list of moves.
	
	ii. After the user uploads a list of moves, we compare the list of moves to the list of moves in the move table.
	
	iii. First, we compare the first move. If the first move is found, then we compare the next move with the next move in that move list for that game. If there is a match, the process is repeated.

	iv. If there is not a match, you check an index for a game_id after that move. You check the next move for every game_id in that index that has an entry for the move_id that you are looking for.

	v. This process continues until there is 1) a move that doesn't match any tree or 2) the moves, in the uploaded game, run out. If the moves in the uploaded game runout, you pull that game id, and store it to the users profile (from the user_groups_table), the game_id should have the name of the game and a link to the prior game_id in the groups_table. Thus, the game can be rebuilt by recursively pulling all of the child game ids.

	vi. if some of the last moves in the game are not in the moves table, a new game_id is created and they are added to the moves table.


	vii. Each user gets 5 new "game_ids" and 10 "comments" in beta. The number of comments and game_ids are tracked in the users table. It is suggested that the user only create game_ids based off of the common opening game_ids, but they are also allowed to upload comepletely off the wall games.

	viii. a comment_table (user, comment, move_id, parent_id) is provided, wherein a comment can be attached to a move and/or a parent comment. Depending on the view, the user can see all of the comments, or only particular users comments.

	ix. a comments_table (comment_id, upvote (user_id), downvote (user_id)) associates a comment with the users upvotes downvotes. Whenever a page is loaded with comments, the upvotes for that comment are counted and the downvote for the comment are counted.


	*The above structure in the database is optimized by having each game being a list of moves, and lists of moves that are duplicated when the user uploads belong to a group_table rather than a particular user. 
	*The user then has the option of looking at their own particular notes, or looking at the groups notes.

	*The group's notes need to be ordered based on upvotes.

D. View Opening Flow - Allows a user to view and comment regarding the opening.

E. Homepage RSS feed - view the latest comments and game_ids. Whenever a game is uploaded, an image of the first move in the game_id or the move_id is uploaded in the RSS feed, and the last item in the RSS feed is removed. I think a time and date should be uploaded in the description as well.

F. MyChessbook Flow - Allows a user to view the lessons that I think are important.
<h1>Old Goals (3/1/2016)</h1>
Right now the site can accomplish four tasks through a sequence of files
1) create a user (ends with show_user)
2) authenticate the user (ends with show_user)
3) view a lesson (ends with show_lesson)
4) create a lesson for personal use (ends with show_lesson)

For the site to be truly done, creating lessons needs to be simple and not limited to openings, so other content creators can add to the site.

I'd like a homepage that shows content recently added to the site.

I guess this is really a social network about chess, but the content is limited to a specific format.

The TOC pages right now, should be limited to what you created in one TOC file, and what others created in another TOC file. You get to choose what you want in your TOC file for what others created.

One question is do I want to create "bot" functionality where you can

5) create bot
6) play against bot

Another question is do I want to create "pvp" functionality, where you can

7) play against another player

Another question is do I want to create "analysis" functionality, where you can

8) create scripts that highlight sqaures in certain situations
9) play with those scripts on in pvp


6/8/2016 Update
Backend is just about done, and I have started to work on frontend.
After considering pulling in two different older versions of the website, I have decided to go for an OO approach.
There will be one "game_obj" created each show_game with three sub-objects: (1) a chessboards_obj, (2) a piecespositions_obj, and (3) a moves_obj. Each object contains an array organized by move_counter_id, a display method, and a set method. Specifically, the set methods are for setting the color of a (1) space, (2) a piece, or (3) a move_cell in the respective objects.

The game_obj has methods for incrementing and decrementing the move_counter, causing the sub-objects to display accordingly 

Once the classes are complete, I can start creating "scripts" for chess lessons and analysis. A script takes in one of the three sub-objects and updates data items in the corresponding array. A script can be activate for a single move_counter by using the next_move button or for all move_counters by hitting the play button.

6/15/2016 Update
The frontend is done. All that is needed is the last backend steps to make sure the flow makes sense.
The flow at this point is:
People click on an opening (Ruy Lopez, Italian, Sicilian) or search a game, and they are encouraged to comment on the next line of moves that they want to talk about. When a comment gets a certain number of upvotes, I will manually add that line to the DB, so people can comment on it. Then I will generate a link to the new thread as a reply to the comment that received the upvotes, so people can talk about that line in a separate thread.

I will choose a person at random to be a "mod", that can delete comments and submit new games to the system.
