<?php

require_once '../../backend/Views/display_nav.php';
require_once '../../backend/Views/display_menu.php';
require_once '../../backend/Views/display_messages.php';

?>

<html>
<head>
	<link href='../../frontend/css/backend_styles.css' rel='stylesheet'>
	<link href='../../frontend/css/frontendStyles.css' rel='stylesheet'>
	<script src='../../frontend/js/boardsObj.js'></script>
	<script src='../../frontend/js/piecesObj.js'></script>
	<script src='../../frontend/js/movesObj.js'></script>
	<script src='../../frontend/js/gameObj.js'></script>

	<script type="text/javascript">
  (function(e,t){var n=e.amplitude||{_q:[]};var r=t.createElement("script");r.type="text/javascript";
  r.async=true;r.src="https://d24n15hnbwhuhn.cloudfront.net/libs/amplitude-2.12.1-min.gz.js";
  r.onload=function(){e.amplitude.runQueuedFunctions()};var s=t.getElementsByTagName("script")[0];
  s.parentNode.insertBefore(r,s);function i(e,t){e.prototype[t]=function(){this._q.push([t].concat(Array.prototype.slice.call(arguments,0)));
  return this}}var o=function(){this._q=[];return this};var a=["add","append","clearAll","prepend","set","setOnce","unset"];
  for(var u=0;u<a.length;u++){i(o,a[u])}n.Identify=o;var c=function(){this._q=[];return this;
  };var p=["setProductId","setQuantity","setPrice","setRevenueType","setEventProperties"];
  for(var l=0;l<p.length;l++){i(c,p[l])}n.Revenue=c;var d=["init","logEvent","logRevenue","setUserId","setUserProperties","setOptOut","setVersionName","setDomain","setDeviceId","setGlobalUserProperties","identify","clearUserProperties","setGroup","logRevenueV2","regenerateDeviceId"];
  function v(e){function t(t){e[t]=function(){e._q.push([t].concat(Array.prototype.slice.call(arguments,0)));
  }}for(var n=0;n<d.length;n++){t(d[n])}}v(n);e.amplitude=n})(window,document);

  amplitude.init("aa66a583c400d1e4816ae38e68c089d7");
</script>
<script src='../../frontend/js/analytics.js'></script>
	<title>Welcome to ChessClubHub</title>
</head>
<body>
	<div id="content_container">
		<div id="header"><img src="../../frontend/img/ChessClubHub.png" width="200" height="50" alt="header" /></div>
		<nav id="sidebar"> <?php display_nav(); ?></nav> <!--end #sidebar-->
		<div id="mainContent">
			<?php 
			display_menu($link);
			display_messages($_SESSION['success_message'], $_SESSION['failure_message']); 
			//clear the messages after showing them
			$_SESSION['success_message'] = NULL;
			$_SESSION['failure_message'] = NULL;
			?>
			<main id="main"></main>
			<aside id="aside">This is a test</aside>
			<button onclick="game.lastMove()">Last</button>
			<button onclick="game.Reset()">Reset</button>
			<button onclick="game.nextMove()">Next</button>


					
		<br class="clearfloat" />
		<?php echo $thread; ?>
		<script>
			var moves = <?php echo $moves; ?>;
			var game = new gameObj(moves);
			game.display();
		</script>
	
		<?php echo $form; ?>
		<br class="clearfloat" />
		</div> <!--end mainContent-->
		<div id="footer">
			<p id="copyright">copyright 2016 ChessClubHub Services, Inc. All rights reserved.</p>
		</div> <!-- end #footer -->
	</div><!--end content_container-->
</body>
</html