<?php

function display_menu($link=NULL){
	echo <<<EOD
		<div id="menu">
		<ul>
			
EOD;

	if (isset($_SESSION['user_id'])){
		echo "<li><a href='/user'>My Profile</a></li>";
		if (user_in_group($link, $_SESSION['user_id'], "Administrators")){
			echo "<li><a href='/users'>Manage Users</a></li>";
		}
		echo "<li><a href='/clear'>Sign Out</a></li>";
	} else {
		echo "<li><a href='/auth'>Sign In</a></li>";
	}
	echo <<<EOD
	</ul>
	</div>
EOD;
}
?>