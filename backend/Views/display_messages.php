<?php

require_once '../../backend/Controllers/app_config.php';
define ("SUCCESS_MESSAGE", "success");
define ("FAILURE_MESSAGE", "failure");

function display_messages($success_msg= NULL, $failure_msg = NULL){
	echo "<div id='messages'>\n";
	if(!is_null($success_msg)){
		display_message($success_msg, SUCCESS_MESSAGE);
	}
	if(!is_null($failure_msg)){
		display_message($failure_msg, FAILURE_MESSAGE);
	}
	echo "</div>\n\n";
}

function display_message($msg, $msg_type){
	echo "<div class='{$msg_type}'>\n";
	echo "<p>{$msg}</p>\n";
	echo " </div>\n";
}
?>