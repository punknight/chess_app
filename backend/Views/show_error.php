<?php

	require_once '../Controllers/app_config.php';
	require_once '../../backend/Views/display_nav.php';
	

	if(isset($_REQUEST['error_message'])){
		$error_message = preg_replace('/\\\\/', '', $_REQUEST['error_message']);
	} else {
		$error_message = "Something went wrong and that is how you ended up here.";
	}

	if (isset($_REQUEST['system_error_message'])){
		$system_error_message = preg_replace("/\\\\/", '', $_REQUEST['system_error_message']);
	} else {
		$system_error_message = "No system-level error message was reported.";
	}

?>

<html>
<head>
	<link href="../../frontend/css/backend_styles.css" rel="stylesheet" />
	<title>Welcome to ChessHub</title>
	<script type="text/javascript">
  (function(e,t){var n=e.amplitude||{_q:[]};var r=t.createElement("script");r.type="text/javascript";
  r.async=true;r.src="https://d24n15hnbwhuhn.cloudfront.net/libs/amplitude-2.12.1-min.gz.js";
  r.onload=function(){e.amplitude.runQueuedFunctions()};var s=t.getElementsByTagName("script")[0];
  s.parentNode.insertBefore(r,s);function i(e,t){e.prototype[t]=function(){this._q.push([t].concat(Array.prototype.slice.call(arguments,0)));
  return this}}var o=function(){this._q=[];return this};var a=["add","append","clearAll","prepend","set","setOnce","unset"];
  for(var u=0;u<a.length;u++){i(o,a[u])}n.Identify=o;var c=function(){this._q=[];return this;
  };var p=["setProductId","setQuantity","setPrice","setRevenueType","setEventProperties"];
  for(var l=0;l<p.length;l++){i(c,p[l])}n.Revenue=c;var d=["init","logEvent","logRevenue","setUserId","setUserProperties","setOptOut","setVersionName","setDomain","setDeviceId","setGlobalUserProperties","identify","clearUserProperties","setGroup","logRevenueV2","regenerateDeviceId"];
  function v(e){function t(t){e[t]=function(){e._q.push([t].concat(Array.prototype.slice.call(arguments,0)));
  }}for(var n=0;n<d.length;n++){t(d[n])}}v(n);e.amplitude=n})(window,document);

  amplitude.init("aa66a583c400d1e4816ae38e68c089d7");
</script>
</head>
<body>
	
	<div id="content_container">
		<div id="header"><img src="../../frontend/img/ChessClubHub.png" width="200" height="50" alt="header" /></div>
		<div id="sidebar"> <?php display_nav(); ?></div> <!--end #sidebar-->
		<div id="mainContent">
			<main id='main'>
			<h1>We're really sorry</h1>
			<p><img src="../../frontend/img/error.jpg" class="error" /> 
			<span class="error_message"><?php echo $error_message; ?></span></p>
			<p> Don't worry though. We have been notified there is a problem, and we take these things seriously.
				In fact, if you want to contact us to find out more about what happened or if you have
				any concerns, you can just email us at <a href="punknight16@gmail.com"> email us</a> 
				and we'll get back to you. </p>
			<p>In the mean time, if you want to go back to the page that caused the problem, you can
				do that <a href="javascript:histoy.go(-1);">by clicking here.</a> If the same problem 
				occurs you may want to come back later. Sorry for the inconvenience.</p>
		<?php 
			debug_print("<hr />");
			debug_print("<p>The following system-level message was received: <b>{$system_error_message}</b></p>");		
		?>
		<br class="clearfloat" />
		</main>
		</div> <!--end mainContent-->
		<div id="footer">
			<p id="copyright">copyright 2016 ChessClubHub Services, Inc. All rights reserved.</p>
		</div> <!-- end #footer -->
	</div><!--end content_container-->
</body>
</html