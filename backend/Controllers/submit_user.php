<?php
require_once '../../backend/Models/submit_user.php';
$main = <<<EOD
  <script>logEvent('submitUserLoaded');</script>
  <link href="../../frontend/css/jquery.validate.password.css" rel="stylesheet" />
  <script src="../../frontend/js/jquery.js"></script>
  <script src="../../frontend/js/jquery.validate.js"></script>
  <script src="../../frontend/js/jquery.validate.password.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#signup_form").validate({
        rules: {
          password: {
            minlength: 6
          },
          confirm_password: {
            minlength: 6,
            equalTo: "#password"
          }
        },
        messages: {
          password: {
            minlength: "too short"
          },
          confirm_password: {
            minlength: "too short",
            equalTo: "unmatched"
          }
        }
      });
    });
  </script>

  <h4>Join the Chess Club Hub</h4>
    
    <form id="signup_form" action="../../backend/Controllers/create_user.php" 
          method="POST" enctype="multipart/form-data">
      <fieldset>
        <label for="username">Username:</label> 
        <input type="text" name="username" size="20" class="required" /><br />
                
        <label for ='password'>Password:</label> 
        <input type="password" id="password" name="password" 
               size="20" class="required password" /> <br />
        
      	
        <label for="confirm_password">Confirm Password:</label> 
        <input type="password" id="confirm_password" name="confirm_password" 
               size="20" class="required" /><br />
        <label for="email">E-Mail Address:</label> 
        <input type="text" name="email" size="30" class="required email" /><br />
        <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
        <label for="user_pic">Upload a picture:</label> 
        <input type="file" name="user_pic" size="30" /><br />
        <label for="bio">Bio:</label> 
        <textarea name="bio" cols="40" rows="10"></textarea>
      </fieldset>
      
      <fieldset class="center">
        <input type="submit" value="Join the Club" />
        <input type="reset" value="Clear and Restart" />
      </fieldset>
    </form>
EOD;

require '../../backend/Views/html_shell.php';
?>