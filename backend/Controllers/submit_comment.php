<?php
require '../../backend/Models/submit_comment.php';
$main = "<script>logEvent('submitCommentLoaded');</script>";

if($move_id==0){
	$main .= "<h3>Bug Report Submission</h3>";
	$main .= "<p>Make a bug report in the box below:</p>";
} else {
	$main .= "<h3>Comment Submission</h3>";
	$main .= "<p>Enter a comment on your chess game in the box below:</p>";
}

$main .= "<form action='../../backend/Controllers/create_comment.php?move_id={$move_id}' method='POST'>";
$main .= "<fieldset>";
$main .= "<textarea id='query_text' name='comment' cols='65' rows='8'></textarea>";
$main .= "<input type='hidden' name='move_id' value='{$move_id}'>";
$main .= "<input type='hidden' name='user_id' value='{$user_id}'>";
$main .= "<input type='hidden' name='timestamp' value='{$timestamp}'>";
$main .= "</fieldset>";
$main .= "<br /><fieldset class='center'>";
$main .= "<input type='submit' value='Submit Comment' />&nbsp";
$main .= "<input type='reset' value='Clear and Restart' />";
$main .= "</fieldset></form>";


require '../../backend/Views/html_shell.php';

?>