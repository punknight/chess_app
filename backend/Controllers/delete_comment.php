<?php

require_once '../../backend/Controllers/app_config.php';
require_once '../../backend/Controllers/database_connection.php';

//get the user id of the user to delete
$comment_id = $_REQUEST['comment_id'];
$move_id = $_REQUEST['move_id'];
//Build the DELETE SQL
$delete_sql = "UPDATE comment_table SET comment='[deleted]' WHERE comment_id={$comment_id};";

//Run it
$result = mysqli_query($link, $delete_sql);
$msg = "The comment you specified has been deleted.";
header("Location: ../../backend/Controllers/show_game.php?move_id={$move_id}&success_message={$msg}");
exit();
?>