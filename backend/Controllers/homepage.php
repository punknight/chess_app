<?php
require '../../backend/Controllers/app_config.php';

$main = "<script>logEvent('homePageLoaded');</script>";
$main .= "<h3 class='center'> Welcome to the Beta!</h3>";

$main .= "<a href='http://chess.loc/backend/Controllers/show_game.php?move_id={RUY_LOPEZ}'>";
$main .= 	"<h4> Ruy Lopez <p class='photo floatRight'>";
$main .=	"<img src='../../frontend/img/ChessHub_example.png' width='150' height='130' alt='Ruy Lopez' />Jump to Ruy Lopez.</p></h4></a>";
$main .= "<p>The Ruy Lopez (1.e4 e5, 2.Nf3 Nc3, 3.Bb5 ...) is designed to put early pressure on the Black knight at Nc6. ";
$main .= "The pressure can result in the knight failing to defend the e5 pawn, either in an <b>exchange</b> or a <b>pin</b>, so the white knight can take the pawn at 4. Nxe5 ...</p><br class='clearfloat' />";

$main .= "<a href='http://chess.loc/backend/Controllers/show_game.php?move_id={ITALIAN_GAME}'>";
$main .= 	"<h4> Italian Game <p class='photo floatRight'>";
$main .= 	"<img src='../../frontend/img/ChessHub_example.png' width='150' height='130' alt='italian game' /> Jump to Italian Game. </p></h4></a>";
$main .= "<p>The Italian Game (1.e4 e5, 2.Nf3 Nc3, 3.Bc4 ...) is designed to put early pressure on Black's f7 pawn. The White knight can eventually move to 7.g5 to get a <b>bridge</b> at 8.Nxf7 or a <b>check</b> at 8.Bxf7.</p>
			<br class='clearfloat' />";
			
$main .= "<a href='http://chess.loc/backend/Controllers/show_game.php?move_id={OPEN_SICILIAN}'>";
$main .= 	"<h4> Open Sicilian <p class='photo floatRight'> <img src='../../frontend/img/ChessHub_example.png' width='150" height='130' alt="open sicilian' /> Jump to Sicilian. </p></h4></a>";
$main .= "<p>The Open Sicilian (1.e4 e5, 2.Nf3 Nc3, 3.Bb5 ...) is an asymmetrical game designed to put white off balance. While White is given the opportunity to advance the 'e4' pawn forward, black is developing a position to <b>stack attack</b> with an eventual 7. ... Qc7 and 8. ... Bd6 or 8. ... Rc8</p>";

require '../../backend/Views/html_shell.php';
?>