<?php

require_once '../../backend/Models/delete_user.php';

//Build the DELETE SQL
$delete_sql = "DELETE FROM users WHERE user_id={$user_id};";

//Run it
$result = mysqli_query($link, $delete_sql);
$success_message = "The user you specified has been deleted.";
$_SESSION['success_message'] = $success_message;
header("Location: ../../backend/Controllers/show_users.php");
exit();
?>