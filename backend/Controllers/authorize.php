<?php

//define(VALID_USERNAME, "admin");
//define(VALID_PASSWORD, "super_secret");

function authorize_user($link, $groups = NULL){
	//no need to check groups if cookies are not set
	session_start();
	if(
		(!isset($_SESSION['user_id'])) || 
		(!strlen($_SESSION['user_id']))
		//||($_SERVER['PHP_AUTH_USER'] != VALID_USERNAME) ||
		//($_SERVER['PHP_AUTH_PW'] != VALID_PASSWORD)
		
	){
		header('Location: /auth');
		exit("You need a valid username and password to be here.");
	}

	//if no groups passed in just return
	if((is_null($groups)) || (empty($groups))){
		return;
	} else {
		//set up the query string
		$query_string = "SELECT ug.user_id FROM user_groups ug, groups g ";
		$query_string .= "WHERE g.name = '%s' ";
		$query_string .= "AND g.id = ug.group_id ";
		$query_string .= "AND ug.user_id=". $_SESSION['user_id'].";";

		foreach ($groups as $group){
			$query = sprintf($query_string, $group);
			$result = mysqli_query($link, $query);
			
			if(mysqli_num_rows($result)==1){
				//give them access
				return;
			} else {
				require_once '../../backend/Controllers/app_config.php';
				error_redirection(
					"You are not authorized to see this page", 
					"User ".$_SESSION['user_id']. " is not authorized to see this page"
				);	 
			}
		}
	}
}

function user_in_group($link, $user_id, $group){
	session_start();
	$query_string = "SELECT ug.user_id FROM user_groups ug, groups g ";
	$query_string .= "WHERE g.name = '%s' ";
	$query_string .= "AND g.id = ug.group_id ";
	$query_string .= "AND ug.user_id=%d;";


	$query = sprintf($query_string, $group, $user_id);
	
	$result = mysqli_query($link, $query);
	if(mysqli_num_rows($result)>0){
		return true;
	} else {
		return false;
	}
}
?>