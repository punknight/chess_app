<?php
	require_once '../../backend/Controllers/app_config.php';
	require_once '../../backend/Controllers/database_connection.php';
	session_start();
	$user_id = trim($_SESSION['user_id']);
	$ELO = trim($_REQUEST['ELO']);
	$timestamp = trim($_REQUEST['timestamp']);

	$insert_sql = "INSERT INTO ELO_table (user_id, ELO, timestamp) ";
	$insert_sql .= "VALUES ({$user_id}, {$ELO}, '{$timestamp}');";

	$user_error_message = "We had trouble saving your ELO in the database.";
	mysqli_query($link, $insert_sql) or error_redirection($user_error_message, mysqli_error($link));
	header("Location: /user");
	exit();
?>