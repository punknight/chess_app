<?php

require '../../backend/Models/show_users.php';

$main = "<script>logEvent('showUsersLoaded');</script>";
$main .= "<script> function delete_user(user_id){";
$main .= "if(confirm('Are you sure you want to delete this user?')){";
$main .= "window.location = '../../backend/Controllers/delete_user.php?user_id='+user_id;";
$main .= "}}</script>";


$main .= "<ul>";
			
	while($user = mysqli_fetch_array($result)){
		$user_row = "<li><a href='show_user.php?user_id={$user['user_id']}'>";
		$user_row .= "{$user['first_name']} {$user['last_name']}</a> ";
		$user_row .= "(<a href = 'mailto:{$user['email']} '>{$user['email']}</a>) ";
		$user_row .= "<a href='javascript:delete_user({$user['user_id']});'> ";
		$user_row .= "<img class='delete_user' src='../../frontend/img/delete.png' width='15' /></a></li>";
				
		$main .= $user_row;
	}
$main .= "</ul>"; 
require '../../backend/Views/html_shell.php';
?>