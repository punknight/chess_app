<?php

//set up debug mode
define("DEBUG_MODE", true);

//site root
define("SITE_ROOT", "/");

define("HOST_WWW_ROOT", "/public_html/");

//Remote Database connection constants
/*
define('DATABASE_HOST', 'punknight.domaincommysql.com');
define('DATABASE_USERNAME', 'punknight');
define('DATABASE_PASSWORD', 'Lauren16');
define('DATABASE_NAME', 'chesshub');
define('DATABASE_PORT', 3306);
define('RUY_LOPEZ', 5);
define('ITALIAN_GAME', 6);
define('OPEN_SICILIAN', 10);
*/

//Local database connection constants

define('DATABASE_HOST', 'localhost');
define('DATABASE_USERNAME', 'root');
define('DATABASE_PASSWORD', 'root');
define('DATABASE_NAME', 'dmillerDB');
define('DATABASE_PORT', 3306);
define('RUY_LOPEZ', 28);
define('ITALIAN_GAME', 27);
define('OPEN_SICILIAN', 32);



function debug_print($message){
	if(DEBUG_MODE){
		return $message;
	}
}

function error_redirection($user_error_message, $system_error_message){
	$error_messages = "Location: ".SITE_ROOT."backend/Views/show_error.php?error_message={$user_error_message}";
	$error_messages .= "&system_error_message={$system_error_message}";
	header($error_messages);
	exit();
}
?>