<?php
require_once '../../backend/Models/submit_search.php';
$main = "<script>logEvent('submitSearchLoaded');</script>";
$main .= "<h3>Search for Games</h3>";
$main .= "<p>Enter an opening in the box below:</p>";
$main .= "<form action='../../backend/Controllers/find_games.php' method='POST'>";
$main .= "	<fieldset>";
$main .= "		<textarea id='query_text' name='query' cols='65' rows='8'>";
$main .= "		</textarea>";
$main .= "	</fieldset>";
$main .= "	<br />";
$main .= "	<fieldset class='center'>";
$main .= "		<input type='submit' value='Run Query' />";
$main .= "		<input type='reset' value='Clear and Restart' />";
$main .= "	</fieldset>";
$main .= "</form>";

require '../../backend/Views/html_shell.php';

?>