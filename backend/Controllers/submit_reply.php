<?php

require_once '../../backend/Models/submit_reply.php';

$main = "<script>logEvent('submitReplyLoaded');</script>";
$main .= "<h3>Reply Submission</h3>";
$main .= "<li class='comment'>{$comment}</li>";
$main .= "<p>Enter a reply to the comment above:</p>";
$main .= "<form action='../Controllers/create_reply.php' method='POST'>";
$main .= "<fieldset>";
$main .= "<textarea id='query_text' name='reply' cols='65' rows='8'></textarea>";
$main .= "<input type='hidden' name='move_id' value='{$move_id}'>";
$main .= "<input type='hidden' name='user_id' value='{$user_id}'>";
$main .= "<input type='hidden' name='timestamp' value={$timestamp}>";
$main .= "<input type='hidden' name='parent_id' value='{$comment_id}'>";
$main .= "</fieldset><br />";
$main .= "<fieldset class='center'>";
$main .= "	<input type='submit' value='Submit Comment' />";
$main .= "	<input type='reset' value='Clear and Restart' />";
$main .= "</fieldset></form>";

require '../../backend/Views/html_shell.php';

?>