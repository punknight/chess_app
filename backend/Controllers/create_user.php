<?php
	require_once '../../backend/Controllers/app_config.php';
	require_once '../../backend/Controllers/database_connection.php';
	require_once '../../backend/Models/create_user.php';

	$insert_sql = "INSERT INTO users (username, password, email, bio) ";
	$insert_sql .= "VALUES('{$username}', '{$password}', '{$email}', '{$bio}');";

	$user_error_message = "We had trouble saving your profile in the database.";
	$system_error_message = mysqli_errno($link);
	mysqli_query($link, $insert_sql) or error_redirection($user_error_message, $system_error_message);

	header("Location: ../Controllers/show_user.php?user_id=".mysqli_insert_id($link));
	exit();
?>