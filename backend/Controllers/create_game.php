<?php
require_once '../../backend/Controllers/app_config.php';
require_once '../../backend/Controllers/database_connection.php';
require_once '../../backend/Models/create_game.php';

$last_move_id = 0;
$parent_id = 0;

//create a game

$insert_sql = "BEGIN;";
$result = mysqli_query($link, $insert_sql);

//determine whether each move in the game_model should be added to the move_table
foreach($game_model as $key => $value){
	//for each move try to find the move based on move_number and parent move ($parent_id)
	$trimmed_value = trim($value);

	$read_sql = "SELECT move_id FROM move_table ";
	$read_sql .= "WHERE move='{$trimmed_value}' AND move_number={$key} AND parent_id={$parent_id} LIMIT 1;";
	$read_result = mysqli_query($link, $read_sql);
	//if the move is found set the parent_id to that move for then next search;
	if(mysqli_num_rows($read_result)){
		while ($row = mysqli_fetch_row($read_result)) {
    	    $parent_id = $row[0];
    	    $last_move_id = $parent_id;
    	}
    } else {
	//if the move is not found insert it into the move_table
		
		$insert_sql = "INSERT INTO move_table(move_number, move, parent_id) ";
		$insert_sql .= "VALUES ({$key}, '{$trimmed_value}', '{$parent_id}');";	
		$result = mysqli_query($link, $insert_sql);
		if ($result) {
    		$parent_id = mysqli_insert_id($link);
    		$last_move_id = $parent_id;
		} else {
			die("it failed");
		}
	// repeat until moves are gone.
	}
}

//after going through all the moves, clean up.	
$insert_sql = "COMMIT;";
$result = mysqli_query($link, $insert_sql);
//$insert_sql = "SELECT game_id FROM move_table where move_id=LAST_INSERT_ID();";
//$result = mysqli_query($link, $insert_sql);
if (!$result){
	die("create_game transaction didn't work");
}
header('Location: ../Controllers/submit_comment.php?move_id='.$last_move_id);
exit();
?>