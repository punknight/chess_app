<?php
require_once '../../backend/Models/show_game.php';

$game_model[0]= NULL;

$counter = count($backward_model)-1;
foreach($backward_model as $key => $value){
	$game_model[$key] = $backward_model[$counter];
	$counter--;
}
$gamelength = count($game_model);
$counter = 1;
$moves = "[";
foreach ($game_model as $key => $value) {
	$moves .= "'{$value}'";
	if($counter != $gamelength){
		$moves .= ", ";
	}
	$counter++;
}
$moves .= " ]";

require '../../backend/Controllers/show_comments.php';
require '../../backend/Controllers/submit_link.php';
require '../../backend/Views/show_game.php';
?>