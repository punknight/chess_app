<?php
require_once '../../backend/Models/submit_authorization.php';

//if the user is logged in, the user_id cookie will be set

if(!isset($_SESSION['user_id'])){
	//See if the login form was submitted with a username pull un and pw
		
		$main = "<script>logEvent('submitAuthorizationLoaded');</script>";
		$main .= "<h3>Sign into the Club</h3>";
		$main .= "<form id='signin_form' action='/auth' method='POST'>";
		$main .= "<fieldset>";
		$main .= "<label for='username'>USERNAME: </label>";
		$main .= "<input type='text' name='username' id='username' size='20'"; 
		$main .= "value='";
		if(isset($username)) $main .= $username; 
		$main .= "' />";
		$main .= "	<br />";
		$main .= "		<label for='password'>PASSWORD: </label>";
		$main .= "		<input type='password' name='password' id='password' size ='20' />";
		$main .= "	</fieldset>";
		$main .= "	<br />";
		$main .= "	<fieldset class='center'>";
		$main .= "		<input type='submit' value='Sign In' />";
		$main .= "	</fieldset>";
		$main .= "</form>";

		require '../../backend/Views/html_shell.php';
	


}else {
//redirect if they are signed in	
	header("Location: ../../backend/Controllers/show_user.php");
	exit();
} 
?>