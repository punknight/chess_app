<?php

	$link = mysqli_init();
	$success = mysqli_real_connect(
	   $link, 
	   DATABASE_HOST, 
	   DATABASE_USERNAME, 
	   DATABASE_PASSWORD, 
	   DATABASE_NAME,
	   DATABASE_PORT
	);

	if(!$success){
		$user_error_message = "There was a problem connecting to the database ";
		$user_error_message .= "which holds necessary information to build the page.";
		$system_error_message = mysqli_connect_errno();
		error_redirection($user_error_message, $system_error_message);
	}
?>