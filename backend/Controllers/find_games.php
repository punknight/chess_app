<?php
require_once '../../backend/Controllers/app_config.php';
require_once '../../backend/Controllers/database_connection.php';
require_once '../../backend/Models/create_game.php';


$last_move_id = 0;
$parent_id = 0;

//determine whether each move in the game_model is in the move_table
foreach($game_model as $key => $value){
	//for each move try to find the move based on move_number and parent move ($parent_id)
	$trimmed_value = trim($value);

	$read_sql = "SELECT move_id FROM move_table ";
	$read_sql .= "WHERE move='{$trimmed_value}' AND move_number={$key} AND parent_id={$parent_id} LIMIT 1;";
	$read_result = mysqli_query($link, $read_sql);
	//if the move is found set the parent_id to that move for then next search;
	if(mysqli_num_rows($read_result)){
		while ($row = mysqli_fetch_row($read_result)) {
    	    $parent_id = $row[0];
    	    $last_move_id = $parent_id;
    	}
    } else {
	//if some moves are not found, send a message later

	} // repeat until moves are gone.
}
if($parent_id==0){
	header('Location: ../../backend/Controllers/submit_game.php');
	exit();
} else{
	header('Location: ../../backend/Controllers/show_games.php?parent_id='.$last_move_id);
	exit();
}
?>