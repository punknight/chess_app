
<?php
require '../../backend/Models/show_comments.php';



function thread_toString($link, $comment_arr, $parent_arrs, $move_id) {
	$thread = '<ul>';
	session_start();

	foreach ($comment_arr as $key => $value) {
		$thread .= "<li class='comment'>{$value}";
		$thread .= "<a href='../../backend/Controllers/submit_reply.php?comment_id={$key}";
		$thread .= "&move_id={$move_id}'>";
		$thread .= "<img src='../../frontend/img/reply.png' width='15' /></a>";
		if (user_in_group($link, $_SESSION['user_id'], "Administrators")){
			$thread .= "<a href='../../backend/Controllers/delete_comment.php?comment_id={$value}";
			$thread .= "&move_id={$move_id}'>";
			$thread .= "<img src='../../frontend/img/delete.png' width='15' /></a>";
		}

		if(isset($parent_arrs[$key])){
			$thread .= thread_toString($link, $parent_arrs[$key], $parent_arrs, $move_id);
		}
		$thread .= "</li>";

	}
	$thread .= "</ul>";
    return $thread;
}

$thread = thread_toString($link, $parent_arr[0], $parent_arr, $move_id); // call the function


?>