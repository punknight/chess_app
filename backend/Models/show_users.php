<?php
	require_once '../../backend/Controllers/app_config.php';
	require_once '../../backend/Controllers/database_connection.php';
	require_once '../../backend/Controllers/authorize.php';
	
	authorize_user($link, array('Administrators'));
	session_start();

	//Build Select Statement
	$select_user = "SELECT user_id, first_name, last_name, email FROM users;";

	//run query
	$result = mysqli_query($link, $select_user);
?>