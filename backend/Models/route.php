<?php

class Route {
	private $_uri = array();

	//builds a collection of internal URL's to look for
	public function add($uri){
		$this->_uri[] = '/'.trim($uri, '/');
	}

	public function submit(){
		$uriGetParam = isset($_GET['uri']) ? $_GET['uri'] : '/';
		$uriGetParam = '/'.trim($uriGetParam, '/');

		
		if ($uriGetParam=='/submit'){
			require '../../backend/Controllers/submit_comment.php';
		}
		if ($uriGetParam=='/test'){
			require '../../backend/Controllers/test.php';
		}
		if ($uriGetParam=='/new'){
			require '../../backend/Controllers/submit_user.php';
		}
		if ($uriGetParam=='/search'){
			require '../../backend/Controllers/submit_search.php';
		}
		if ($uriGetParam=='/'){
			require '../../backend/Controllers/homepage.php';
		}
		if ($uriGetParam=='/clear'){
			require '../../backend/Controllers/clear_authorization.php';
		}
		if ($uriGetParam=='/auth'){
			require '../../backend/Controllers/submit_authorization.php';
		}
		if ($uriGetParam=='/user'){
			require '../../backend/Controllers/show_user.php';
		}
		if ($uriGetParam=='/users'){
			require '../../backend/Controllers/show_users.php';
		}
		if ($uriGetParam=='/ELO'){
			require '../../backend/Controllers/create_ELO.php';
		}
	}
}
?>