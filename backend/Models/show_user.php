<?php
require_once '../../backend/Controllers/app_config.php';
require_once '../../backend/Controllers/database_connection.php';
require_once '../../backend/Controllers/authorize.php';

//authorize_user($link);
session_start();
$user_id = 1;
$timestamp = date('Y-m-d G:i:s');
if(isset($_SESSION['user_id'])){
  $user_id = $_SESSION['user_id'];
}
$select_query = "SELECT * FROM users WHERE user_id ={$user_id};";
$result = mysqli_query($link, $select_query);

if(!$result){
  $user_error_message = "There was a problem finding your information in the system.";
  $system_error_message = "Error locating user_id={$user_id}";
  error_redirection($user_error_message, $system_error_message);

} else {
  $row = mysqli_fetch_array($result);
  $username = $row['username'];
  $bio = preg_replace("/[\r\n]+/", "</p><p>", $row['bio']);
  $email = $row['email'];
  $user_image = str_replace($_SERVER['DOCUMENT_ROOT'], '', $row['user_pic_path']);
}
//get max elo
$select_maxELO = "SELECT MAX(ELO) FROM ELO_table WHERE user_id ={$user_id};";
$ELOresult = mysqli_query($link, $select_maxELO);
$row = mysqli_fetch_array($ELOresult);
$maxELO = $row[0];

//get current ELO
$select_currentELO = "SELECT ELO FROM ELO_table WHERE user_id ={$user_id} ORDER BY timestamp DESC LIMIT 1;";
$ELOresult = mysqli_query($link, $select_currentELO);
$row = mysqli_fetch_array($ELOresult);
$currentELO = $row[0];
?>