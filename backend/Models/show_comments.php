<?php
require_once '../../backend/Controllers/app_config.php';
require_once '../../backend/Controllers/database_connection.php';
require_once '../../backend/Controllers/authorize.php';

$move_id = '1';
if (isset($_REQUEST['move_id'])){
	$move_id = $_REQUEST['move_id'];
}
$counter=0;
$comment_arr= array();
$parent_arr = array();

$query_text = "SELECT ct.comment, u.username, ct.comment_id, ct.parent_id FROM comment_table ct, users u "; 
$query_text .= "WHERE move_id={$move_id} ";
$query_text .= "AND ct.user_id = u.user_id;";
$results = mysqli_query($link, $query_text);

//do a join query to pull the user name instead of user_id when you 're-learn it' from the php book
	
if(!$results){
	die("<p>Error in executing the SQL query ". $query_text. ": " . mysqli_error($link)."</p>");
} else {

	while ($result = mysqli_fetch_array($results)) {
    	$parent_arr[$result['parent_id']][$result['comment_id']] = "<b>".$result['username']."</b> ".$result['comment'];
    }
}


?>