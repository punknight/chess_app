function movesObj(moves) {
	var turns = [
        //no highlight at state 0
        [0]
    ];
    var div = Math.ceil(moves.length/2);
        var k = 0;
        for (i=0; i < div; i++){
            turns.push([0, 0]);
            turns[i+1][0] = moves[k];
            moves.push(0);
            k++;
            turns[i+1][1] = moves[k];
            k++;
        }

    this.display = function (state) {
       document.getElementById("aside").innerHTML = '';

       var x = document.createElement("table");
       x.setAttribute("id", "turntable");
       document.getElementById("aside").appendChild(x);
    	var k = 0;
    	for (var i = 0; i < turns.length; i++) { 
            var y = document.createElement("tr");
            var turn = "t" + i;
            y.setAttribute("id", turn);
            document.getElementById("turntable").appendChild(y);

			for (var j = 0; j < turns[i].length; j++) { 
            	
                var z = document.createElement("td");
            	var turnID = "t" + i+"m"+j;
            	

                z.setAttribute("id", turnID);
            	
            	if(state == k){
            		z.setAttribute("class", "highlight");
            	}
                 
                if(turns[i][j]!=0){
            	  
                   document.getElementById(turn).appendChild(z);
          
            	   var t = document.createTextNode(turns[i][j]);
                   document.getElementById(turnID).appendChild(t);
                }

    			k++;

        	}
            
        }

    }
	
	this.setTurn = function (transition, row, column, move){
        turns[transition][row][column] = move;
    }
     
}