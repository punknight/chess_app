function piecesObj(moves) {
   
	this.imgArr = new Array();
	this.imgArr[0] = "0";
	this.imgArr['wP'] = "<img src=../../frontend/img/wpawn.png>";
	this.imgArr['wR'] = "<img src=../../frontend/img/wrook.png>";
	this.imgArr['wN'] = "<img src=../../frontend/img/wknight.png>";
	this.imgArr['wB'] = "<img src=../../frontend/img/wbishop.png>";
	this.imgArr['wQ'] = "<img src=../../frontend/img/wqueen.png>";
	this.imgArr['wK'] = "<img src=../../frontend/img/wking.png>";
	this.imgArr['bP'] = "<img src=../../frontend/img/bpawn5.png>";
	this.imgArr['bR'] = "<img src=../../frontend/img/brook.png>";
	this.imgArr['bN'] = "<img src=../../frontend/img/bknight.png>";
	this.imgArr['bB'] = "<img src=../../frontend/img/bbishop.png>";
	this.imgArr['bQ'] = "<img src=../../frontend/img/bqueen.png>";
	this.imgArr['bK'] = "<img src=../../frontend/img/bking.png>";
	
    //generate initial state positions
    this.positions = [
        //pieces positions at state 0
        [
            //piece positions at state 0 by row: row 1, row 2, etc.
            [this.imgArr['wR'], this.imgArr['wN'], this.imgArr['wB'], this.imgArr['wQ'], this.imgArr['wK'], this.imgArr['wB'], this.imgArr['wN'], this.imgArr['wR']], 
            [this.imgArr['wP'], this.imgArr['wP'], this.imgArr['wP'], this.imgArr['wP'], this.imgArr['wP'], this.imgArr['wP'], this.imgArr['wP'], this.imgArr['wP']],
            [this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0]], 
            [this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0]], 
            [this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0]], 
            [this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0], this.imgArr[0]], 
            [this.imgArr['bP'], this.imgArr['bP'], this.imgArr['bP'], this.imgArr['bP'], this.imgArr['bP'], this.imgArr['bP'], this.imgArr['bP'], this.imgArr['bP']],
            [this.imgArr['bR'], this.imgArr['bN'], this.imgArr['bB'], this.imgArr['bQ'], this.imgArr['bK'], this.imgArr['bB'], this.imgArr['bN'], this.imgArr['bR']]
        ]
        //etc.
    ];
    //define methods
    this.findPiece = findPiece;
    this.findRow = findRow;
    this.findColumn = findColumn;
    this.setPosition = setPosition;
    this.displayPosition = displayPosition;

    this.smartErase = smartErase;

    //for each additional state push a copy of the board
    for (i=0; i < moves.length; i++){
        
        this.positions.push([
            [this.positions[i][0][0].slice(0), this.positions[i][0][1].slice(0), this.positions[i][0][2].slice(0), this.positions[i][0][3].slice(0), this.positions[i][0][4].slice(0), this.positions[i][0][5].slice(0), this.positions[i][0][6].slice(0), this.positions[i][0][7].slice(0)],
            [this.positions[i][1][0].slice(0), this.positions[i][1][1].slice(0), this.positions[i][1][2].slice(0), this.positions[i][1][3].slice(0), this.positions[i][1][4].slice(0), this.positions[i][1][5].slice(0), this.positions[i][1][6].slice(0), this.positions[i][1][7].slice(0)],
            [this.positions[i][2][0].slice(0), this.positions[i][2][1].slice(0), this.positions[i][2][2].slice(0), this.positions[i][2][3].slice(0), this.positions[i][2][4].slice(0), this.positions[i][2][5].slice(0), this.positions[i][2][6].slice(0), this.positions[i][2][7].slice(0)],
            [this.positions[i][3][0].slice(0), this.positions[i][3][1].slice(0), this.positions[i][3][2].slice(0), this.positions[i][3][3].slice(0), this.positions[i][3][4].slice(0), this.positions[i][3][5].slice(0), this.positions[i][3][6].slice(0), this.positions[i][3][7].slice(0)],
            [this.positions[i][4][0].slice(0), this.positions[i][4][1].slice(0), this.positions[i][4][2].slice(0), this.positions[i][4][3].slice(0), this.positions[i][4][4].slice(0), this.positions[i][4][5].slice(0), this.positions[i][4][6].slice(0), this.positions[i][4][7].slice(0)],
            [this.positions[i][5][0].slice(0), this.positions[i][5][1].slice(0), this.positions[i][5][2].slice(0), this.positions[i][5][3].slice(0), this.positions[i][5][4].slice(0), this.positions[i][5][5].slice(0), this.positions[i][5][6].slice(0), this.positions[i][5][7].slice(0)],
            [this.positions[i][6][0].slice(0), this.positions[i][6][1].slice(0), this.positions[i][6][2].slice(0), this.positions[i][6][3].slice(0), this.positions[i][6][4].slice(0), this.positions[i][6][5].slice(0), this.positions[i][6][6].slice(0), this.positions[i][6][7].slice(0)],
            [this.positions[i][7][0].slice(0), this.positions[i][7][1].slice(0), this.positions[i][7][2].slice(0), this.positions[i][7][3].slice(0), this.positions[i][7][4].slice(0), this.positions[i][7][5].slice(0), this.positions[i][7][6].slice(0), this.positions[i][7][7].slice(0)]
        ]);
    
        //parse the move from the moves[i]
        var piece = 'w';
        if(i%2==1){
            piece = 'b';
        }
        piece += this.findPiece(moves[i]); 
        //piece might be z for kside or y for qside
        //so now the output is bz, wz, by, wy
        var row = this.findRow(moves[i]); 
        //row might by 9 for kside or qside
        //so now the output is 9 for row
        var column = this.findColumn(moves[i]);
        //a is 0 so y is 24 and z is 25

        if (row == 8 || row == 9){
            if (piece == 'bz'){
                this.setPosition((i+1), 7, 6, 'bK');
                this.setPosition((i+1), 7, 5, 'bR');
                this.setPosition((i+1), 7, 4, '0');
                this.setPosition((i+1), 7, 7, '0');    
            }
            if (piece == 'wz'){
                this.setPosition((i+1), 0, 6, 'wK');
                this.setPosition((i+1), 0, 5, 'wR');
                this.setPosition((i+1), 0, 4, '0');
                this.setPosition((i+1), 0, 7, '0');    
            }
            if (piece == 'by'){
                this.setPosition((i+1), 7, 2, 'bK');
                this.setPosition((i+1), 7, 3, 'bR');
                this.setPosition((i+1), 7, 0, '0');
                this.setPosition((i+1), 7, 4, '0');    
            }
            if (piece == 'wy'){
                this.setPosition((i+1), 0, 2, 'wK');
                this.setPosition((i+1), 0, 3, 'wR');
                this.setPosition((i+1), 0, 0, '0');
                this.setPosition((i+1), 0, 4, '0');    
            }
        } else {
            //draw the piece on the board based on the moves[i]
            this.setPosition((i+1), row, column, piece);
            //erase the old piece based off of the move
            this.smartErase((i+1), row, column, piece, moves[i]);
        }   
    }

    //Methods that the GameObj will call
    function displayPosition (state) {
    	for (i = 0; i <= 7; i++) {
    		for (j = 0; j <= 7; j++) { 
    			var spaceID = "r" + i+"c"+j;
    			if(this.positions[state][i][j]!=0){
    				document.getElementById(spaceID).innerHTML = this.positions[state][i][j];
    			}
    		}
    	}
    }


    function setPosition (state, row, column, piece){
    	//alert("state: "+state+" row: "+row+" column: "+column+" piece: "+piece);
        this.positions[state][row][column] = this.imgArr[piece];
    }


    //Methods necessary to figure out how to add a piece
    function findRow (move){
    	var row = /[1-9]$/.exec(move);
    	row[0] = row[0]-1;
    	return row[0];
    }
    
    function findColumn (move){
    	var end = /[a-hyz][1-9]*$/.exec(move);
    	var column = /[a-hyz]/.exec(end);
    	column[0] = column[0].charCodeAt(0);
    	column[0] = column[0]-97;
    	return column[0];
    }
    function findPiece (move){
		var piece_char = /^[RNBQKzy]/.exec(move);
		if(piece_char == null){
			piece_char = "P";
		}
		return piece_char;
    }
    //methods necessary to figure out how to erase a peice
    function smartErase (state, row, column, piece, move){
        if(piece=='wP'){
            if(this.positions[state][row-1][column]==this.imgArr[piece]){
                this.positions[state][row-1][column] = this.imgArr[0];
            }
            if(this.positions[state][row-2][column]==this.imgArr[piece]){
                this.positions[state][row-2][column] = this.imgArr[0];
            }
            var attackcheck = /[a-h]x/.exec(move);
            if (attackcheck != null){
               var attackcheck = /^[a-h]/.exec(attackcheck[0]); 
                attackcheck[0] = attackcheck[0].charCodeAt(0);
                attackcheck[0] = attackcheck[0]-97;
                if(this.positions[state][row-1][attackcheck[0]]==this.imgArr[piece]){
                    this.positions[state][row-1][attackcheck[0]] = this.imgArr[0];
                }
            }

        }
        if(piece=='bP'){
            if(this.positions[state][row+1][column]==this.imgArr[piece]){
                this.positions[state][row+1][column] = this.imgArr[0];
            }
            if(this.positions[state][row+2][column]==this.imgArr[piece]){
                this.positions[state][row+2][column] = this.imgArr[0];
            }
            var attackcheck = /[a-h]x/.exec(move);
            if (attackcheck != null){
               var attackcheck = /^[a-h]/.exec(attackcheck[0]); 
                attackcheck[0] = attackcheck[0].charCodeAt(0);
                attackcheck[0] = attackcheck[0]-97;
                if(this.positions[state][row+1][attackcheck[0]]==this.imgArr[piece]){
                    this.positions[state][row+1][attackcheck[0]] = this.imgArr[0];
                }
            }
        }
        if(findPiece(move)=='N'){
            if(typeof this.positions[state][row-2] !== "undefined"){
                if(this.positions[state][row-2][column-1]==this.imgArr[piece]){
                    this.positions[state][row-2][column-1] = this.imgArr[0];
                }
                if(this.positions[state][row-2][column+1]==this.imgArr[piece]){
                    this.positions[state][row-2][column+1] = this.imgArr[0];
                }
            }
            if(typeof this.positions[state][row+2] !== "undefined"){
                if(this.positions[state][row+2][column+1]==this.imgArr[piece]){
                    this.positions[state][row+2][column+1] = this.imgArr[0];
                }
                if(this.positions[state][row+2][column-1]==this.imgArr[piece]){
                    this.positions[state][row+2][column-1] = this.imgArr[0];
                }
            }
         
            if(typeof this.positions[state][row-1] !== "undefined"){
                if(this.positions[state][row-1][column-2]==this.imgArr[piece]){
                    this.positions[state][row-1][column-2] = this.imgArr[0];
                }
                if(this.positions[state][row-1][column+2]==this.imgArr[piece]){
                this.positions[state][row-1][column+2] = this.imgArr[0];
                }
            }
            if(typeof this.positions[state][row+1] !== "undefined"){
                if(this.positions[state][row+1][column+2]==this.imgArr[piece]){
                    this.positions[state][row+1][column+2] = this.imgArr[0];
                }
                if(this.positions[state][row+1][column-2]==this.imgArr[piece]){
                    this.positions[state][row+1][column-2] = this.imgArr[0];
                }
            }
        }
        if(findPiece(move)=='B'||findPiece(move)=='Q'){
            for(i=1; i<=7; i++){
                if(typeof this.positions[state][row-i] !== "undefined"){
                   if(this.positions[state][row-i][column-i]==this.imgArr[piece]){
                        this.positions[state][row-i][column-i] = this.imgArr[0];
                        break;
                    }
                    if(this.positions[state][row-i][column+i]==this.imgArr[piece]){
                        this.positions[state][row-i][column+i] = this.imgArr[0];
                        break;
                    } 
                }
                if(typeof this.positions[state][row+i] !== "undefined"){
                   if(this.positions[state][row+i][column-i]==this.imgArr[piece]){
                        this.positions[state][row+i][column-i] = this.imgArr[0];
                        break;
                    }
                    if(this.positions[state][row+i][column+i]==this.imgArr[piece]){
                        this.positions[state][row+i][column+i] = this.imgArr[0];
                        break;
                    } 
                }
            }
        }
        if(findPiece(move)=='R'||findPiece(move)=='Q'){
            for(i=1; i<=7; i++){
                if(this.positions[state][row][column-i]==this.imgArr[piece]){
                    this.positions[state][row][column-i] = this.imgArr[0];
                    break            
                }
                if(this.positions[state][row][column+i]==this.imgArr[piece]){
                    this.positions[state][row][column+i] = this.imgArr[0];
                    break            
                }
                if(typeof this.positions[state][row-i] !== "undefined"){
                    if(this.positions[state][row-i][column]==this.imgArr[piece]){
                        this.positions[state][row-i][column] = this.imgArr[0];
                        break            
                    }
                }
                if(typeof this.positions[state][row+i] !== "undefined"){
                    if(this.positions[state][row+i][column]==this.imgArr[piece]){
                        this.positions[state][row+i][column] = this.imgArr[0];
                        break            
                    }
                }
            }
        }
        if(findPiece(move)=='K'){
            if(this.positions[state][row][column-1]==this.imgArr[piece]){
                this.positions[state][row][column-1] = this.imgArr[0];
            }
            if(this.positions[state][row][column+1]==this.imgArr[piece]){
                this.positions[state][row][column+1] = this.imgArr[0];
            }
            if(typeof this.positions[state][row-1] !== "undefined"){
                    for(var i=0; i<3; i++){
                        if(this.positions[state][row-1][column-1+i]==this.imgArr[piece]){
                            this.positions[state][row-1][column-1+i] = this.imgArr[0];
                            break;               
                        }
                    }
                }
            if(typeof this.positions[state][row+1] !== "undefined"){
                for(var i=0; i<3; i++){
                    if(this.positions[state][row+1][column-1+i]==this.imgArr[piece]){
                        this.positions[state][row+1][column-1+i] = this.imgArr[0];
                        break;             
                    }
                }
            }   
        }
        if(findPiece(move)=='z'){
            //king side castle

        }
        if(findPiece(move)=='z'){
            //king side castle
            
        }
    }
}