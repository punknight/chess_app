function stackattack(positions, highlights) {
	this.imgArr = new Array();
	this.imgArr[0] = "0";
	this.imgArr['wP'] = "<img src=../../frontend/img/wPawn.png>";
	this.imgArr['wR'] = "<img src=../../frontend/img/wRook.png>";
	this.imgArr['wN'] = "<img src=../../frontend/img/wKnight.png>";
	this.imgArr['wB'] = "<img src=../../frontend/img/wBishop.png>";
	this.imgArr['wQ'] = "<img src=../../frontend/img/wQueen.png>";
	this.imgArr['wK'] = "<img src=../../frontend/img/wKing.png>";
	this.imgArr['bP'] = "<img src=../../frontend/img/bPawn.png>";
	this.imgArr['bR'] = "<img src=../../frontend/img/bRook.png>";
	this.imgArr['bN'] = "<img src=../../frontend/img/bKnight.png>";
	this.imgArr['bB'] = "<img src=../../frontend/img/bBishop.png>";
	this.imgArr['bQ'] = "<img src=../../frontend/img/bQueen.png>";
	this.imgArr['bK'] = "<img src=../../frontend/img/bKing.png>";


	for(i=0; i<positions.length; i++){
		for(j=0; j<positions[i].length; j++){
			for(k=0; k<positions[i][j].length; k++){
				if(positions[i][j][k]==this.imgArr['wR']||positions[i][j][k]==this.imgArr['wQ']){
					if(positions[i][j+1][k]==this.imgArr['wR']||positions[i][j+1][k]==this.imgArr['wQ']){
						//highlight the entire line
						for(l=0; l<8; l++){
							highlights[i][l][k] = 'red';
						}
					}	
				}
			}
		}
	}
return highlights;
}
