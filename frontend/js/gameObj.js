function gameObj(moves) {
	this.state = 0;
	this.moves = moves;
	this.boards = new boardsObj(this.moves);
	this.pieces = new piecesObj(this.moves);
	this.moves = new movesObj(this.moves);

	document.getElementById("main").innerHTML += "<fieldset class='center'><button onclick='game.lastMove()'>Last</button>";
	document.getElementById("main").innerHTML += "<button onclick='game.Reset()'>Reset</button>";
	document.getElementById("main").innerHTML += "<button onclick='game.nextMove()'>Next</button></fieldset>";
		
		
	
	this.display = function(){
		this.boards.display(this.state);
	    this.pieces.displayPosition(this.state);
	    this.moves.display(this.state);
	}

	this.nextMove = function() {
	     this.state++;
	     this.display();
	     
	}
	this.lastMove = function() {
		this.state--;
		this.display();

	}
	this.Reset = function() {
		this.state=0;
		this.display();
		amplitude.logEvent('ResetGame');
	}
}