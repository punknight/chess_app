function boardsObj(moves){
    
    //create the first board with no highlights
    this.highlights = [
        //board on move 0
        [
            //board at state 0 by row: row 0, row 1, etc.
            [0, 0, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 0, 0, 0]
        ]
        
    ];
    //generate additional boards for each state with no higlights
    for (i=0; i <= moves.length; i++){
        this.highlights.push([
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0]
            ]);        
    }


    this.display = function (state) {
        document.getElementById("main").innerHTML = '';
   
        var x = document.createElement("table");
        x.setAttribute("id", "chessboard");
        document.getElementById("main").appendChild(x);
   
        for (i = 7; i >= 0; i--) { 
            var y = document.createElement("tr");
            var row = "r" + i;
            y.setAttribute("id", row);
            document.getElementById("chessboard").appendChild(y);

            for (j = 0; j <= 7; j++) { 
                var z = document.createElement("td");
                var column = "r" + i+"c"+j;
                z.setAttribute("id", column);
                
                //set background color based on state
                
                if((i+j)%2==1){
                    var highlight = "b" + this.highlights[state][i][j];
                    z.setAttribute("class", highlight);
                } else {
                    var highlight = "w" + this.highlights[state][i][j];
                    z.setAttribute("class", highlight);
                }
                document.getElementById(row).appendChild(z);
            }
        }
    }

    this.setHighlight = function (state, row, column, color){
        this.highlights[state][row][column] = color;
    }
}